# README #

A program to find KDC(Korean Decimal Classification, 한국십진분류법 in Korean).
Communicates with National Library of Korea's website to search the result.
Written with Java.

Usage : 


1. Compile Source


```
#!java

javac src/MainWindow.java && java MainWindow

```


2. Use pre-compiled binary


```
#!java
java -jar KDCFinder.jar
```


Still developing.