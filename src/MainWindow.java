import java.awt.EventQueue;

import javax.swing.JFrame;

import org.jsoup.Jsoup;

import javax.swing.*;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.UnknownHostException;

import org.jsoup.*;
import org.jsoup.nodes.Document;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.Sizes;

public class MainWindow implements ActionListener {

	private JFrame frmKdcFinder;
	private JTextField ISBNValue;
	private JSeparator separator;
	private JLabel lblAuthor;
	private JLabel lblKdc;
	private JTextField AuthorR;
	private JTextField KDCR;
	private JLabel lblBookName;
	private JTextField BookNameR;
	private JButton btnSearch;
	private JButton btnClear;
	private JButton btnHelp;
    String nl = Character.toString((char) 10);

	/**
	 * Launch the application.
	 */
	
	public String[] SearchItem(String ISBN) throws IOException, UnknownHostException {
		
		String cl = Character.toString((char) 34);
        String kdc;
        String bookname;
        String author;
        Document doc;
        String returnarr[] = new String[3];
       
        doc = Jsoup.connect("http://dibrary.net/search/portal/searchStorage.jsp?kwd=" + ISBN).get();
        
        String exists = doc.toString();
        if(exists.contains("<p class=" + cl + cont + cl + ">")) {
            returnarr[0] = "noresult";
            return returnarr;
        }

        else {
            String kdchtml = doc.select("div.explainText_list dd.last-child").get(1).toString().replace("<dd class=" + cl + "last-child" + cl + ">", "");
            kdc = kdchtml.replace("</dd>", "").replace(nl, "");

            String authtml = doc.select("div.explainText_list dd").get(0).toString().replace("<dd>", "");
            author = authtml.replace("</dd>", "").replace(nl, "");

            bookname = doc.select("div.bookName a").text().replace("1. ", "");

            returnarr[0] = bookname;
            returnarr[1] = author;
            returnarr[2] = kdc;
            return returnarr;
        }
		
	}
	
	public void dataProduction() {
		MainWindow m = new MainWindow();
		try {
			if(m.SearchItem(ISBNValue.getText())[0] == "noresult") {
			    ISBNValue.setText("No search result.");
			    BookNameR.setText("");
			    AuthorR.setText("");
			    KDCR.setText("");
				frmKdcFinder.setTitle("KDC Finder");
			} else {
			    String result[] = new String[3];
			    result = m.SearchItem(ISBNValue.getText());
			    BookNameR.setText(result[0]);
			    AuthorR.setText(result[1]);
			    KDCR.setText(result[2]);
			    ISBNValue.setText("");
				Toolkit toolkit = Toolkit.getDefaultToolkit();
				Clipboard clipboard = toolkit.getSystemClipboard();
				StringSelection strSel = new StringSelection(result[2]);
				clipboard.setContents(strSel, null);
				frmKdcFinder.setTitle("KDC Finder - KDC copied to Clipboard");

				
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			JOptionPane.showMessageDialog(null, "Unstable network connection. Try again later.", "Failed", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void noResult() {
		ISBNValue.setText("No search result.");
        BookNameR.setText("");
        AuthorR.setText("");
        KDCR.setText("");
		frmKdcFinder.setTitle("KDC Finder");
	}
	public void clear() {
		ISBNValue.setText("");
        BookNameR.setText("");
        AuthorR.setText("");
        KDCR.setText("");
		frmKdcFinder.setTitle("KDC Finder");
	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmKdcFinder.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKdcFinder = new JFrame();
		frmKdcFinder.setTitle("KDC Finder");
		frmKdcFinder.setBounds(100, 100, 463, 195);
		frmKdcFinder.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKdcFinder.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(129dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				new ColumnSpec(ColumnSpec.FILL, Sizes.bounded(Sizes.DEFAULT, Sizes.constant("30dlu", true), Sizes.constant("40dlu", true)), 0),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(37dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(0dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblSearchItem = new JLabel("Search Item");
		frmKdcFinder.getContentPane().add(lblSearchItem, "2, 4");
		ISBNValue = new JTextField();
		frmKdcFinder.getContentPane().add(ISBNValue, "6, 4, fill, default");
		ISBNValue.setColumns(10);
		ISBNValue.registerKeyboardAction(this, "search", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0), JComponent.WHEN_FOCUSED);
		ISBNValue.registerKeyboardAction(this, "clear", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0), JComponent.WHEN_FOCUSED);
		separator = new JSeparator();
		frmKdcFinder.getContentPane().add(separator, "1, 6, 13, 1");
		
		lblBookName = new JLabel("Book Name");
		frmKdcFinder.getContentPane().add(lblBookName, "2, 8");
		
		BookNameR = new JTextField();
		frmKdcFinder.getContentPane().add(BookNameR, "6, 8, fill, default");
		BookNameR.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.setActionCommand("search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ISBNValue.getText().length() == 13 || ISBNValue.getText().length() == 10) { 
					dataProduction();
				}
				else { 
					JOptionPane.showMessageDialog(null, "Invalid keyword.", "Failed", JOptionPane.ERROR_MESSAGE);
					clear();
				}
			}
		});
		
		
		frmKdcFinder.getContentPane().add(btnSearch, "8, 8");
		
		lblAuthor = new JLabel("Author");
		frmKdcFinder.getContentPane().add(lblAuthor, "2, 10");
		
		AuthorR = new JTextField();
		frmKdcFinder.getContentPane().add(AuthorR, "6, 10, fill, default");
		AuthorR.setColumns(10);
		
		btnClear = new JButton("Clear");
		btnClear.setActionCommand("clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clear();
			}
			
		});
		
		frmKdcFinder.getContentPane().add(btnClear, "8, 10");
		
		lblKdc = new JLabel("KDC");
		frmKdcFinder.getContentPane().add(lblKdc, "2, 12");
		
		KDCR = new JTextField();
		frmKdcFinder.getContentPane().add(KDCR, "6, 12, fill, default");
		KDCR.setColumns(10);
		
		btnHelp = new JButton("Help");
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String helpdialog = "Usage : " + nl + "Type in ISBN code first into Search Item Textbox, then click Search button or press Enter to start searching. " + nl + "Book name, author, KDC code will be searched. " + nl + "Searched code will be automatically copied into your clipboard." + nl + "Press Clear button to clean up all the data.";
				JOptionPane.showMessageDialog(null, helpdialog, "사용법", JOptionPane.PLAIN_MESSAGE); 
			}
		});
		frmKdcFinder.getContentPane().add(btnHelp, "8, 12");
		
		frmKdcFinder.setResizable(false);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getActionCommand() == "search") {
			if(ISBNValue.getText().length() == 13) { 
				dataProduction();
			}
			else { 
				JOptionPane.showMessageDialog(null, "Invalid keyword.", "Failed", JOptionPane.ERROR_MESSAGE);
				clear();
			}
		} else if(e.getActionCommand() == "clear") {
			clear();
		}
	}
	


	
}
